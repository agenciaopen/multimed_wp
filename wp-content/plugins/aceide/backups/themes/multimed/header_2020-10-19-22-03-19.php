<?php /* start AceIDE restore code */
if ( $_POST["restorewpnonce"] === "f64e1349cd54c32e31a12cdac6bc245b0431d57c81" ) {
if ( file_put_contents ( "/home/grupomultimed/www/wp-content/themes/multimed/header.php" ,  preg_replace( "#<\?php /\* start AceIDE restore code(.*)end AceIDE restore code \* \?>/#s", "", file_get_contents( "/home/grupomultimed/www/wp-content/plugins/aceide/backups/themes/multimed/header_2020-10-19-22-03-19.php" ) ) ) ) {
	echo __( "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file." );
}
} else {
echo "-1";
}
die();
/* end AceIDE restore code */ ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <title><?php wp_title(); ?></title>
		<link rel="icon" type="image/png" href="https://www.grupomultimed.com.br/wp-content/uploads/2020/08/favicon.png" />
		<?php if(is_404()): ?>
			<meta name="description" content="Não encontramos essa página. Navegue pelo site e encontre mais informações sobre nosso sistema acessível à saúde.">
		<?php endif; ?>

        <?php wp_head(); ?>

    </head>


<body <?php body_class(); ?> id="multimed">
    <div class="top-bar pt-2 pb-2">
        <div class="container-fluid">
            <div class="row">
                <a href="https://acesso.siprov.com.br/area-cliente/login.jsf?id=100" target="_blank"><i class="far fa-user"></i> Área do cliente</a>
                <a href="https://acesso.siprov.com.br/area-medico/login.jsf" target="_blank"><i class="far fa-user"></i> Área do parceiro</a>
                <a href="https://acesso.siprov.com.br/area-medico/login.jsf" target="_blank"><i class="fas fa-phone-alt"></i> (31) 3243-1958</a>
            </div>
        </div>
    
    </div>
    <nav class="navbar navbar-expand-xl navbar-default fixed-top" role="navigation" id="nav_main">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="icon-bar top-bar"></span>
              <span class="icon-bar middle-bar"></span>
              <span class="icon-bar bottom-bar"></span>
          </button>
       
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
        </a>
        
            <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav ml-auto align-items-center',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ) );
            ?>
        </div>
    </nav>