<?php /* start AceIDE restore code */
if ( $_POST["restorewpnonce"] === "f64e1349cd54c32e31a12cdac6bc245bbafa41a75e" ) {
if ( file_put_contents ( "/home/grupomultimed/www/wp-content/themes/multimed/template-page-contact.php" ,  preg_replace( "#<\?php /\* start AceIDE restore code(.*)end AceIDE restore code \* \?>/#s", "", file_get_contents( "/home/grupomultimed/www/wp-content/plugins/aceide/backups/themes/multimed/template-page-contact_2021-02-04-20-44-01.php" ) ) ) ) {
	echo __( "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file." );
}
} else {
echo "-1";
}
die();
/* end AceIDE restore code */ ?><?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section id="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-5 text-center">
                <?php the_field( 'texto_da_pagina', $pageID ); ?>
            </div>
            <div class="col-md-6 text-center">
                <?php 
                    $phone = get_field( 'numero_whatsapp', $pageID );
                    $phone = preg_replace('/\D+/', '', $phone);
                    $message = rawurldecode(get_field( 'mensagem_ao_abrir_whatsapp', $pageID ));
                ?>
                <a href="https://wa.me/<?php echo $phone; ?> rel="external" target="_blank">
                    <img src='<?php the_field( 'icone_whatsapp', $pageID ); ?>' class='img-fluid' alt='<?php echo $phone; ?>?text=<?php echo $message;?>' title='<?php echo $phone; ?>?text=<?php echo $message;?>' loading='lazy'>
                    <p><b><?php the_field( 'texto_apos_whatsapp', $pageID ); ?></b></p>
                </a>
            </div>
			<div class="col-md-8 mt-5 text-center content">
			<p class="mb-4"> <?php the_field( 'form_de_contatomsg', $pageID ); ?></p>
			                <?php the_field( 'form_de_contato', $pageID ); ?>

			</div>
        </div>
    </div>
</section><!--/.content-->

<?php get_footer(); ?>