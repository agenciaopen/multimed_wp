<?php /* start AceIDE restore code */
if ( $_POST["restorewpnonce"] === "f64e1349cd54c32e31a12cdac6bc245bdfb42bedb8" ) {
if ( file_put_contents ( "/home/grupomultimed/www/wp-content/themes/multimed/template-page-home.php" ,  preg_replace( "#<\?php /\* start AceIDE restore code(.*)end AceIDE restore code \* \?>/#s", "", file_get_contents( "/home/grupomultimed/www/wp-content/plugins/aceide/backups/themes/multimed/template-page-home_2021-01-06-20-12-02.php" ) ) ) ) {
	echo __( "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file." );
}
} else {
echo "-1";
}
die();
/* end AceIDE restore code */ ?><?php
/**
*
* Template Name: Home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<div id="carousel" class="carousel slide" data-ride="carousel">
    
    <?php if ( have_rows( 'cadastro_de_slider', $page_ID ) ) : ?>
        <ol class="carousel-indicators">
            <?php $count = 0;?>
            <?php while ( have_rows( 'cadastro_de_slider', $page_ID ) ) : the_row(); ?>
                <li data-target="#carousel" data-slide-to="<?php echo $count;?>" class="<?php if($count == 0):?> active <?php endif;?>"></li>
            <?php $count++; endwhile; ?>
        </ol>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
    <div class="carousel-inner">
        <?php if ( have_rows( 'cadastro_de_slider', $page_ID ) ) : ?>
            <?php $count = 0;?>
            <?php while ( have_rows( 'cadastro_de_slider', $page_ID ) ) : the_row(); ?>
                <div class="carousel-item <?php if($count == 0):?> active <?php endif;?>" style="background-image: url('<?php the_sub_field( 'imagem_de_fundo' ); ?>')">
                    <div class="d-flex h-100 align-items-end justify-content-center">
                        <div class="container row m-0 justify-content-center">
                            <?php if ( have_rows( 'cadastrar_texto_ou_icones' ) ): ?>
                                <?php while ( have_rows( 'cadastrar_texto_ou_icones' ) ) : the_row(); ?>
                                    <?php if ( get_row_layout() == 'texto' ) : ?>
                                        <div class="col-md-8 col-lg-7">
                                            <h1><?php the_sub_field( 'titulo' ); ?></h1>
                                            <h2><?php the_sub_field( 'descricao', false ,false ); ?></h2>
                                        </div>
                                 
                                    <?php elseif ( get_row_layout() == 'icones' ) : ?>
                                        <?php if ( have_rows( 'cadastro_de_icones' ) ) : ?>
                                            <?php while ( have_rows( 'cadastro_de_icones' ) ) : the_row(); ?>
                                                <div class="col-6 col-md-3 text-center">
                                                    <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                                        <img src='<?php the_sub_field( 'imagem' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'descricao' ); ?>' title='<?php the_sub_field( 'descricao' ); ?>' loading='lazy'>
                                                    <?php endif ?>
                                                    <p><?php the_sub_field( 'descricao' ); ?></p>
                                                </div>
                                               
                                            <?php endwhile; ?>
                                        <?php else : ?>
                                            <?php // no rows found ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php else: ?>
                                <?php // no layouts found ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php $count++; endwhile; ?>
        <?php else : ?>
            <?php // no rows found ?>
        <?php endif; ?>
    </div>
    <a class=" d-none carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class=" d-none carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

    <section class="content plans pb-0">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-8 col-lg-7 text-center">
                                <h2> <?php the_field( 'titulo_planos', $page_ID ); ?></h2>
                                <p> <?php the_field( 'descricao_planos', false, false, $page_ID ); ?></p>
                            </div>
                            <div class="col-md-12 col-lg-12">
                                <div class="row m-0 justify-content-between align-items-start <?php if(wp_is_mobile()):?> carousel_featured <?php endif;?> ">
                                    <?php 
                                $planos = get_posts( array(
                                    'posts_per_page' => -1,
                                    'post_type' => 'planos'
                                ) );
                                if ( $planos ) {
                                    foreach ( $planos as $post ) :
                                        setup_postdata( $post ); ?>
                                    <div class="col-md-6 mb-5 mb-lg-0 col-lg-4 px-md-4 text-center text-md-left item mt-5 <?php if ($page_ID == $post->ID): ?> active <?php endif; ?>">
                                        <div class="col-md-12 p-0 card px-md-4">
                                            <div class="col-md-12 icon p-0">
                                                <img src='<?php the_field( 'icone_plano', $post->ID ); ?>' class='img-fluid' alt='' title='' loading='lazy'>
                                            </div>
                                            <h3><?php the_field( 'subtitulop', $post->ID); ?></h3>
                                            <?php if ( have_rows( 'beneficios_gerais' ) ) : ?>
                                                <ul class="benefits">
                                                    <?php while ( have_rows( 'beneficios_gerais' ) ) : the_row(); ?>
                                                        <li><span><?php the_sub_field( 'textob' ); ?></span></li> 
                                                    <?php endwhile; ?>
                                                    <?php if (get_field('observacao')): ?>
                                                        <li class="obs"><small><?php the_field( 'observacao' ); ?></small></li>
                                                    <?php endif;?>
                                                </ul>
                                            <?php else : ?>
                                                <?php // no rows found ?>
                                            <?php endif; ?>
                                            <?php if ( have_rows( 'cadastro_valores' ) ) : ?>
                                                <div class="list-inline p-0 m-0 row justify-content-between valores-plano-<?php echo $plan_type++; ?>">
                                                    <?php while ( have_rows( 'cadastro_valores' ) ) : the_row(); ?>
                                                        <div class="col-md-5 p-0">
															
                                                            <span class="badge">
                                                                <?php the_sub_field( 'tipo_plano' ); ?>
                                                            </span>
                                                            <div class="row m-0 align-items-end justify-content-center">
                                                                <div class="col-2 col-md-4 p-0">
                                                                    <p class="installments"><?php the_sub_field( 'parcelas' ); ?>x</p>
                                                                    <p class="sign">R$</p>
                                                                </div>
                                                                <div class="col-3 col-md-8 p-0">
                                                                    <p class="price"><?php the_sub_field( 'valor_parcelado' ); ?></p>
                                                                </div>
                                                                <div class="col-12 p-0 ml-4 text-center">
																	
                                                                    <p><small><?php the_sub_field( 'metodo_de_pagamento' 																	  ); ?> </small></p>
																	
                                                                </div>
                                                            </div>
                                            </div>
                                                    <?php endwhile; ?>
                                                </div>
                                            <?php else : ?>
                                                <?php // no rows found ?>
                                            <?php endif; ?>
                                    
                                        </div>
                                         <?php if ( get_field( 'iframe' ) == 1 ) : ?>
                                            <?php $botao = get_field( 'botao' ); ?>

                                            <?php // echo 'true'; ?>
                                            <div class="pointer mt-4" data-toggle="modal">
                                                <div class="btn col-md-12 btn_default mt-5">
                                                    <a href="https://acesso.siprov.com.br/siprov-web/pub/100/saude/adesaoContrato.jsf" target="_blank"><?php echo esc_html( $botao['title'] ); ?></a>

                                                </div> 
                                            </div>
                                        <?php else : ?>
                                            <?php // echo 'false'; ?>
                                            <?php $botao = get_field( 'botao' ); ?>
                                                <?php if ( $botao ) : ?>
                                                    <a clas="mt-4" href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                                                        <div class="btn col-md-12 btn_default mt-5">
                                                            <?php echo esc_html( $botao['title'] ); ?>

                                                        </div> 
                                                    </a>
                                                <?php endif; ?> 
                                            <?php endif; ?>  
                                    </div>
    <?php
    endforeach; 
    wp_reset_postdata();
}

?>
                                    
                                </div>
                            </div>
           
                        </div>
                        <div class="row mt-4 h-100">
                            <div class="col-md-12 text-center mt-4">
                                <?php $descricao_planos_copiar = get_field( 'descricao_planos_copiar', $page_ID ); ?>
                                <?php if ( $descricao_planos_copiar ) : ?>
                                    <a  class="js-scroll-trigger" href="<?php echo esc_url( $descricao_planos_copiar['url'] ); ?>" target="<?php echo esc_attr( $descricao_planos_copiar['target'] ); ?>">
                                        <div class="btn btn_first mt-5">
                                            <?php echo esc_html( $descricao_planos_copiar['title'] ); ?>

                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section><!-- /.content -->
				<section class="form_lead content" id="target">
    <div class="container h-100 toggle_form mt-5" id="form_toggle">
	<div class=" closes pointer col-md-12" id="toggle_form">
        <svg width="102" height="30" viewBox="0 0 102 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M77.1743 0.845348C76.0471 -0.281783 74.2197 -0.281783 73.0926 0.845348C71.9654 1.97248 71.9654 3.79992 73.0926 4.92705L82.9575 14.792L72.8453 24.9041C71.7182 26.0312 71.7182 27.8587 72.8453 28.9858C73.9725 30.113 75.7999 30.113 76.927 28.9858L87.0392 18.8737L97.0726 28.9071C98.1997 30.0342 100.027 30.0342 101.154 28.9071C102.281 27.7799 102.281 25.9525 101.154 24.8254L91.1209 14.792L100.907 5.00582C102.034 3.87869 102.034 2.05125 100.907 0.924115C99.7799 -0.203017 97.9525 -0.203019 96.8254 0.924113L87.0392 10.7103L77.1743 0.845348Z" fill="#7CD8FF"></path>
            
        </svg>
    </div>
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10">
                <?php echo do_shortcode('[contact-form-7 id="402" title="Solução para Associação de Proteção Veicular"]'); ?>
            </div>
        </div>
    </div>
</section><!-- /.form_lead -->
<?php
    $featured_img_url = get_field( 'imagem_de_fundo' ,$page_ID); 
?>
<?php if( get_field('titulo_revendedor', $page_ID) ): ?>
    <?php $title = get_field('titulo_revendedor', $page_ID); ?>
<?php else: ?>
<?php endif; ?>

<section class="main middle" style="background-image:url('<?php echo $featured_img_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12 col-lg-9 text-center">
                <h2><?php echo $title;?></h2>
                <h3><?php the_field( 'descricao_revendedor', $page_ID, false, false); ?></h3>
                <?php $botao_revendedor = get_field( 'botao_revendedor' ); ?>
                <?php if ( $botao_revendedor ) : ?>
                    <a href="<?php echo esc_url( $botao_revendedor['url'] ); ?>" target="<?php echo esc_attr( $botao_revendedor['target'] ); ?>">
                        <div class="btn btn_first">
                            <?php echo esc_html( $botao_revendedor['title'] ); ?>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.main -->

<section class="content redes">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10 col-lg-8 text-center">
                <h2><?php the_field( 'titulo_redes', $page_ID ); ?></h2>
                <p><?php the_field( 'descricao_redes', $page_ID ); ?></p>
            </div>
        </div>
        <div class="row h-100 align-items-center justify-content-center carousel_featured_redes ">

        <?php 
                        	$redes = get_posts( array(
                                'posts_per_page' => -1,
                                'post_type' => 'redes'
                            ) );
                            if ( $redes ) {
                                $count = 0;
                                foreach ( $redes as $post ) :
                                    setup_postdata( $post ); ?>
                                    <?php 
                                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post->ID),'full'); 
                                        $title_rede = get_the_title($post->ID);
                                    ?>
                                    <div class="col text-center item_rede">
                                        <img src='<?php echo $featured_img_url;?>' class='img-fluid' alt='<?php echo $title_rede;?>' title='<?php echo $title_rede;?>' loading='lazy'>

                                    </div>
                                        
                                <?php
                                $count++; endforeach; 
                                wp_reset_postdata();
                            }
                            ?>
            
        </div>
        <div class="row h-100 align-items-center">
        <div class="col-md-12 text-center mt-5">
            <?php $botao_redes = get_field( 'botao_redes' ); ?>
            <?php if ( $botao_redes ) : ?>
                <a href="<?php echo esc_url( $botao_redes['url'] ); ?>" target="<?php echo esc_attr( $botao_redes['target'] ); ?>">
                    <div class="btn btn_first">
                        <?php echo esc_html( $botao_redes['title'] ); ?>
                    </div>
                </a>
            <?php endif; ?>
            </div>
            </div>
    </div>
</section><!-- /.content article -->
<?php if ( have_rows( 'cadastro_de_depoimentos' ) ) : ?>
    <section class="content testimonials">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-md-12 text-center">
                    <h2><?php the_field( 'titulo_depoimentos', $page_ID ); ?></h2>
                </div>
                <div class='slider-item container'>
                    <?php while ( have_rows( 'cadastro_de_depoimentos', $page_ID ) ) : the_row(); ?>
                        <div class="col-md-12">
                            <div class="row m-0 justify-content-center align-items-center h-100">
                                <div class="col-md-10 text-center">
                                    <p><?php the_sub_field( 'descricao', false, false ); ?></p>
                                    <p><b><?php the_sub_field( 'nome' ); ?>
                                    <?php the_sub_field( 'cargo' ); ?></b></p>
                                </div>
                            </div>
                            
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section><!-- /.content bg_procedures -->
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
<section class="content article">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10 col-lg-8 text-center">
                <h2><?php the_field( 'titulo_artigos', $page_ID ); ?></h2>
                <p><?php the_field( 'descricao_artigos', $page_ID ); ?></p>
            </div>
        </div>
        <div class="row h-100 align-items-stretch justify-content-center">
            <?php 
            $post = get_posts( array(
                'posts_per_page' => 4,
                'post_type' => 'post'
            ) );
            if ( $post ) {
                foreach ( $post as $post ) :
                    setup_postdata( $post ); ?>
                    <?php 
                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post->ID),'medium'); 
                        $title_rede = get_the_title($post->ID);
                    ?>
                        <div class="col-md-6 col-lg-3 my-4">
                            <div class="card col-md-12 p-0">
                                <div class="card-header p-0" style="background-image: url('<?php echo $featured_img_url;?>');">

                                </div>
                                <div class="card-body text-center text-white">
                                    <p><?php echo get_the_date(); ?></p>
									
											<?php the_category(); ?>
										
                                    <h4 class="mt-3"><?php echo $title_rede;?></h4>
                                    <a href="<?php the_permalink(); ?>">Ler agora</a>
                                </div>
                            </div>
                        </div>
                                
                <?php
                endforeach; 
                wp_reset_postdata();
            }
            ?>
            	
        </div>
    </div>
</section><!-- /.content article -->

<!-- Modal 
<div class="modal fade planos_modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://v7.waysistemas.com:98/scriptcase/app/MultiMED/Blk_Site/Blk_Site.php#" width="100%" frameborder="0"></iframe>
            </div>
        </div>
    </div>
  </div>
</div>-->
<?php get_footer(); ?>