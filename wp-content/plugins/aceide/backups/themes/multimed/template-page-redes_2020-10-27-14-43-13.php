<?php /* start AceIDE restore code */
if ( $_POST["restorewpnonce"] === "f64e1349cd54c32e31a12cdac6bc245be65467fabc" ) {
if ( file_put_contents ( "/home/grupomultimed/www/wp-content/themes/multimed/template-page-redes.php" ,  preg_replace( "#<\?php /\* start AceIDE restore code(.*)end AceIDE restore code \* \?>/#s", "", file_get_contents( "/home/grupomultimed/www/wp-content/plugins/aceide/backups/themes/multimed/template-page-redes_2020-10-27-14-43-13.php" ) ) ) ) {
	echo __( "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file." );
}
} else {
echo "-1";
}
die();
/* end AceIDE restore code */ ?><?php
/**
*
* Template Name: Redes Credenciadas
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<?php
global $post;
$page_ID = $post->ID;
// get page ID
?>

<section class="content redes">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-start">
            <div class="col-md-12 ">
                <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter" class="form-inline">
                    <?php if( $terms = get_terms( 'tyest', 'orderby=name&hide_empty=0' ) ) : ?>
                        <div class="form-group col-md-12 mb-2">
                            <h3 class="col-md-12 p-0">Tipo de estabelecimento</h3>
                            <div class="form-check form-check-inline mb-3 p-0">
                                <input class="styled-checkbox" id="all" name="places" type="radio" value="value1" checked>
                                <label for="all">Todos</label>
                            </div>
                            
                            <?php
                            foreach ( $terms as $term ) : ?>
                                <div class="form-check form-check-inline mb-3 p-0">
                                    <input class="styled-checkbox" id="<?php  echo $term->term_id; ?>" name="places" type="radio" value="<?php  echo $term->term_id ?>">
                                    <label for="<?php  echo $term->term_id; ?>"><?php echo $term->name ?></label>
                                </div>
                            <?php
                            endforeach; ?>
                        </div>
                        <?php endif;?>
                     <?php if( $terms = get_terms( 'tyser', 'orderby=name&hide_empty=0' ) ) : ?>
                        <div class="form-group col-md-12 mb-2">
                            <h3 class="col-md-12 p-0">Tipo de serviço</h3>
                            <div class="form-check form-check-inline mb-3 p-0">
                                <input class="styled-checkbox" id="all2" name="services" type="radio" value="value1" checked>
                                <label for="all2">Todos</label>
                            </div>
                            
                            <?php
                            foreach ( $terms as $term ) : ?>
                                <div class="form-check form-check-inline mb-3 p-0">
                                    <input class="styled-checkbox" id="<?php  echo $term->term_id; ?>" name="services" type="radio" value="<?php  echo $term->term_id ?>">
                                    <label for="<?php  echo $term->term_id; ?>"><?php echo $term->name ?></label>
                                </div>
                            <?php
                             endforeach; ?>
                        </div>
                        <?php endif;?>
                    <button class="btn btn_first col-md-4 mt-4 mx-auto text-center mb-4" id="filter_redes">Filtrar</button>
                    <input type="hidden" name="action" value="myfilter">
                </form> 
            </div>
            <div class="col-md-12 mt-5">
                <div id="response" class="row m-0 justify-content-start ">
                    <?php 
                        	$redes = get_posts( array(
                                'posts_per_page' => -1,
                                'post_type' => 'redes',
								'orderby'=> 'title', 
								'order' => 'ASC'
                            ) );
                            if ( $redes ) {
                                foreach ( $redes as $post ) :
                                    setup_postdata( $post ); ?>
                                    <?php 
                                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post->ID),'full'); 
                                        $title_rede = get_the_title($post->ID);
                                        $address_rede = get_field('endereco', $post->ID);
                                        $phone_rede = get_field('telefone', $post->ID);?>
                                        <?php $tyser = get_the_term_list( $post->ID, 'tyser', '', ' - ', '' ); ?> 
                                        <?php $tyest = get_the_term_list( $post->ID, 'tyest', ' -  ', ' - ', '' ); ?> 

                                        
                                            <div class="col-md-4 col-lg-3 mb-5 item_rede">
                                                <div class="col-md-12 item">
                                                    <img src='<?php echo $featured_img_url;?>' class='img-fluid' alt='<?php echo $title_rede;?>' title='<?php echo $title_rede;?>' loading='lazy'>
                                                    <h4><?php echo $title_rede;?></h4>
                                                    <?php echo $tyser = strip_tags( $tyser );?>
                                                    <?php echo $tyest = strip_tags( $tyest );?>
                                                    <p class="mt-3"><?php echo $address_rede;?></p>
                                                    <p><?php echo $phone_rede;?></p>
                                                </div>
                                               
                                            </div>
                                <?php
                                endforeach; 
                                wp_reset_postdata();
                            }
                            ?>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
<section class="content bg_procedures">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10 col-lg-8 text-center">
                <h2><?php the_field( 'titulo_pr', $page_ID ); ?></h2>
                <p><?php the_field( 'descricao_pc', $page_ID ); ?></p>
                <?php if ( have_rows( 'cadastrodearquivooulink', $page_ID ) ) : ?>
					<?php while ( have_rows( 'cadastrodearquivooulink', $page_ID ) ) : the_row(); ?>
                        <?php if(get_sub_field('arquivo_ou_link') == "arquivo"): ?>
                        <?php $arquivo_botao = get_sub_field( 'arquivo_botao' ); ?>
                        <?php if ( $arquivo_botao ) : ?>
                            <a target="_blank" href="<?php echo esc_url( $arquivo_botao['url'] ); ?>" class="mr-md-3">
                                <div class="btn btn_first">
                                    <?php the_sub_field( 'texto_do_botao_arquivo' ); ?>
                                </div>
                            </a>
                        <?php endif; ?>
                    
                    <?php else : ?>
                        <?php $link_botao_pcr = get_sub_field( 'link_botao_pcr' ); ?>
                        <?php if ( $link_botao_pcr ) : ?>
                            <a href="<?php echo esc_url( $link_botao_pcr['url'] ); ?>" target="<?php echo esc_attr( $link_botao_pcr['target'] ); ?>" class="mr-md-3">
                                <div class="btn btn_first">
                                    <?php echo esc_html( $link_botao_pcr['title'] ); ?>
                                </div>
                            </a>
                        <?php endif; ?>
            
                    <?php endif; ?>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.content bg_procedures -->
<?php get_footer(); ?>
