<?php
/**
 * Plugin Name: multimed
 * Plugin URI: https://multimed.com.br
 * Description: post types plugin
 * Version: 1.0
 * Author: Mateus Lara
 * Author URI: https://www.mateuslara.com.br
 */

 /*
* Creating a function to create our CPT
*/
add_theme_support('post-thumbnails');

function planos_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Planos', 'Post Type General Name', 'multimed' ),
            'singular_name'       => _x( 'Plano', 'Post Type Singular Name', 'multimed' ),
            'menu_name'           => __( 'Planos', 'multimed' ),
            'parent_item_colon'   => __( 'Parent Plano', 'multimed' ),
            'all_items'           => __( 'Todos os Planos', 'multimed' ),
            'view_item'           => __( 'Ver Plano', 'multimed' ),
            'add_new_item'        => __( 'Adicionar novo plano', 'multimed' ),
            'add_new'             => __( 'Adicionar novo', 'multimed' ),
            'edit_item'           => __( 'Editar plano', 'multimed' ),
            'update_item'         => __( 'Atualizar plano', 'multimed' ),
            'search_items'        => __( 'Procurar plano', 'multimed' ),
            'not_found'           => __( 'Nada encontrado', 'multimed' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'multimed' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'planos', 'multimed' ),
            'description'         => __( 'Cadastro de planos e seus detalhes', 'multimed' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
'rewrite' => array(
        'slug' => 'modalidades',
        'with_front' => false
    ),
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-welcome-add-page',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'planos', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'planos_custom_post_type', 0 );

    // Creating a redes Custom Post Type
function redes_custom_post_type() {
	$labels = array(
		'name'                => __( 'Redes' ),
		'singular_name'       => __( 'Rede'),
		'menu_name'           => __( 'Redes'),
		'parent_item_colon'   => __( 'Parent Rede'),
		'all_items'           => __( 'Todas as Redes'),
		'view_item'           => __( 'Ver Rede'),
		'add_new_item'        => __( 'Adicionar nova rede'),
		'add_new'             => __( 'Adicionar nova'),
		'edit_item'           => __( 'Editar rede'),
		'update_item'         => __( 'Atualizar rede'),
		'search_items'        => __( 'Procurar rede'),
		'not_found'           => __( 'Nada encontrado'),
		'not_found_in_trash'  => __( 'Nada encontrado na lixeira')
	);
	$args = array(
		'label'               => __( 'redes'),
		'description'         => __( 'Nossas redes credenciadas'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => false,
        'can_export'          => true,
        'menu_icon'           => 'dashicons-admin-multisite',
		'exclude_from_search' => false,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page'
);
	register_post_type( 'redes', $args );
}
add_action( 'init', 'redes_custom_post_type', 0 );

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'create_redes_custom_taxonomy', 0 );
 
//create a custom taxonomy name it "type" for your posts
function create_redes_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Tipo de estabelecimento', 'taxonomy general name' ),
    'singular_name' => _x( 'Tipo de estabelecimento', 'taxonomy singular name' ),
    'search_items' =>  __( 'Procurar por tipos' ),
    'all_items' => __( 'Todos os tipos' ),
    'parent_item' => __( 'Parent tipo' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Editar tipo' ), 
    'update_item' => __( 'Atualizar tipo' ),
    'add_new_item' => __( 'Adicionar novo tipo de estabelecimento' ),
    'new_item_name' => __( 'Novo tipo de estabelecimento' ),
    'menu_name' => __( 'Tipos de estabelecimento' ),
  ); 	
 
  register_taxonomy('tyest',array('redes'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tipos-de-estabelecimento' ),
  ));
}

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'create_redes_custom_taxonomy_service', 0 );
 
//create a custom taxonomy name it "type" for your posts
function create_redes_custom_taxonomy_service() {
 
  $labels = array(
    'name' => _x( 'Tipo de serviço', 'taxonomy general name' ),
    'singular_name' => _x( 'Tipo de serviço', 'taxonomy singular name' ),
    'search_items' =>  __( 'Procurar por tipos' ),
    'all_items' => __( 'Todos os tipos' ),
    'parent_item' => __( 'Parent tipo' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item' => __( 'Editar tipo' ), 
    'update_item' => __( 'Atualizar tipo' ),
    'add_new_item' => __( 'Adicionar novo tipo de serviço' ),
    'new_item_name' => __( 'Novo tipo de serviço' ),
    'menu_name' => __( 'Tipos de serviço' ),
  ); 	
 
  register_taxonomy('tyser',array('redes'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tipos-de-servico' ),
  ));
}

function misha_filter_function(){
    $args = array(
        'post_type' => 'redes',
		'orderby'=> 'title', 
								'order' => 'ASC'
      
    );
    $relation = 'OR';
    if(!empty($_POST['services']) && isset( $_POST['places'] )) {
        $relation = 'AND';
    }
    if( empty( $_POST['all'] ) ) {

    if( isset( $_POST['places'] ) ||  !empty( $_POST['services']) )
        $args['tax_query'] = array(
            'relation' => $relation, 
            array(
                'taxonomy' => 'tyser',
                'field' => 'id',
                'terms' => $_POST['services']
            ),
            array(
                'taxonomy' => 'tyest',
                'field' => 'id',
                'terms' => $_POST['places'],
            ),
        );
    }
    $query = new WP_Query( $args );

    if( $query->have_posts() ) :
        while( $query->have_posts() ): $query->the_post();?>
        <?php 
                                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post->ID),'full'); 
                                        $title_rede = get_the_title($post->ID);
                                        $address_rede = get_field('endereco', $post->ID);
                                        $phone_rede = get_field('telefone', $post->ID);?>
                                        <?php $tyser = get_the_term_list( $post->ID, 'tyser', '', ' - ', '' ); ?> 
                                        <?php $tyest = get_the_term_list( $post->ID, 'tyest', '', ' - ', '' ); ?> 

                                        
                                        <div class="col-md-4 col-lg-3 item_rede mb-5">
                                                <div class="col-md-12 item">
                                                <img src='<?php echo $featured_img_url;?>' class='img-fluid' alt='<?php echo $title_rede;?>' title='<?php echo $title_rede;?>' loading='lazy'>
                                                <h4><?php echo $title_rede;?></h4>
                                                <?php echo $tyser = strip_tags( $tyser );?>
                                                <?php echo $tyest = strip_tags( $tyest );?>
                                                <p class="mt-3"><?php echo $address_rede;?></p>
                                                <p><?php echo $phone_rede;?></p>
                                            </div>
                                        </div>
        <?php

        endwhile;
        wp_reset_postdata();
    else :
        echo 'Sem resultados';
    endif;

    die();
}


add_action('wp_ajax_myfilter', 'misha_filter_function'); 
add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');