<?php get_header(''); ?>

<?php
global $post;
$pageID = get_option('page_on_front');

?>
	<?php
		$cat = get_query_var('cat');
		$category = get_category ($cat);
	?>
<?php
$queried_object = get_queried_object();
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;
$term_name = $queried_object->term_name;
$enable_cat =  $taxonomy . '_' . $term_id;

?>

<?php
if(wp_is_mobile()):
				

                    $featured_img_url = get_field( 'imagem_destacada', $enable_cat );; 
                else:
   
					   $featured_img_url = get_field( 'imagem_destacada', $enable_cat );;  
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('<?php echo $featured_img_url;?>');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h1>
									<?php echo $category->name; ?>

                                </h1>
                            </div>
                        </div>
                    </div>
                </section>
				<section id="categories" class=" pb-2">
		<div class="container h-100">
			<div class="row m-0 h-100 align-items-center justify-content-between pt-3 pb-3">
				<div class="col-md-4 text-center mb-4 mb-md-0 text-md-left pl-lg-5">
					<h4>
						Categorias
					</h4>
					
				</div>
				<div class="col-md-8 text-center">
					<ul class="list-inline m-0">
						<li class="list-inline-item item mr-lg-5">
								<a href="/blog" class="link font-bariol" title=""> 
									Todos os posts 
									<hr /> 
								</a>
							</li>
						<?php 
							$categories = get_categories( array(
						    	'orderby' => 'name',
						    	'order'   => 'ASC'
							) );

						foreach( $categories as $category ) {
							?>
							
							<li class="list-inline-item item mr-lg-5 <?= $category->slug; ?>">
								<a href="<?php echo get_category_link( $category->term_id ); ?>" class="link font-bariol" title="<?php echo $category->name; ?>"> 
									<?php echo $category->name; ?> 
									<hr /> 
								</a>
							</li>
			
						<?php } ?>
					</ul>
					
				</div>
				
			</div>
		</div>
</section>
                    <section class="list_posts pt-0">
                        <div class="container">
                        <div class="row">
	<?php
		$cat = get_query_var('cat');
		$category = get_category ($cat);
	?>
                    <?php 
				
				
				// WP_Query arguments
				$args = array(
					'nopaging'        => false,
					'paged'           => 'paged',
					'posts_per_page'  => '4',
					'category_name'   => $category->slug
				);

				// The Query
				$posts = new WP_Query( $args );
				$cont = 0;

				// The Loop
				echo do_shortcode('[ajax_load_more container_type="div" post_type="post" category= '.$category->slug.' posts_per_page="4" button_label="Veja mais posts"]'); 

				// Restore original Post Data
				wp_reset_postdata();
			?>
                        </div>                        </div>


                </section><!-- /.main -->
<?php get_footer(); ?>
