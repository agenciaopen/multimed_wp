<?php get_template_part( 'global/template-part', 'newsletter' ); ?>
<!-- Footer -->
<footer>
    <div class="container h-100">
        <div class="row h-100 align-items-stretch justify-content-between">
            <div class="col-md-12 d-none p-0">
                <!-- Navigation -->
                <nav class="navbar navbar-expand-md navbar-default" role="navigation">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'your-theme-slug' ); ?>">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand d-none d-lg-inline-block" href="<?php echo home_url(); ?>">
                            <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
                        </a>
                        <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'primary',
                            'depth'             => 2,
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse',
                            'container_id'      => 'bs-example-navbar-collapse-1',
                            'menu_class'        => 'nav navbar-nav ml-auto align-items-center',
                            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'            => new WP_Bootstrap_Navwalker(),
                        ) );
                        ?>
                    </div>
              
                </nav>
            </div>
            <?php if ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : ?>
                <div class="col-md-12 text-center text-md-right">
              
                    <ul class="list-inline">
                    <?php while ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : the_row(); ?>
                        <?php $link_da_rede_social = get_sub_field( 'link_da_rede_social' ); ?>
                        <?php if ( $link_da_rede_social ) : ?>
                            <li class="list-inline-item">
                                <a rel="external" href="<?php echo esc_url( $link_da_rede_social['url'] ); ?>" target="<?php echo esc_attr( $link_da_rede_social['target'] ); ?>">
                                    <span>
                                        <?php the_sub_field( 'icone' ); ?>
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    </ul>
                </div>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
			<div class="col-md-12 text-center text-md-right">
            <a href="https://www.grupomultimed.com.br/politica-de-privacidade/" target="_blank" rel="noopener noreferrer">Politíca de Privacidade</a>
				<p class="text-white">
                
					<small>Desenvolvido por <a href="https://agenciaopen.com" target="_blank">
						<img src="/wp-content/themes/multimed/img/open.svg" class="ml-2"></small>
					</a>
				</p>
			</div>
        </div>
    </div>
</footer>

  
<?php wp_footer(); ?>

</body>

</html>
