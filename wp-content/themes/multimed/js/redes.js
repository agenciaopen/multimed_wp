$(document).ready(function () {
    var sliderNav = $('.carousel_featured_redes');

    if(sliderNav.children('.item_rede').length >= 4) {
        var dots_active = true;
    }else{
        var dots_active = false;

    }
    $('.carousel_featured_redes').slick({
        
        dots: dots_active,
        slidesToShow: 4,
        infinite: true,
        slidesToScroll: 4,
        infinite: true,
		
  
        responsive: [
            {
                breakpoint: 767,
                settings: {
					dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            }
        ]
    });
	
	$('.plans .js-scroll-trigger .btn, #toggle_form').click(function() {
		$('#form_toggle').toggle('slow');
	});
	// Add smooth scrolling to all links
  $("a.js-scroll-trigger").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        //window.location.hash = hash;
      });
    } // End if
  });

});


