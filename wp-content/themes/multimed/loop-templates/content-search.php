<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
		<div class="row m-0 mt-5 align-items-center h-100 item w-100">
                    <div class="col-md-12 card">
                        <div class="card-body p-0">
                            <div class="row m-0 align-items-center">
                                <div class="col-md-8 pl-lg-5">
                                    <div class="col-md-12">
                                        <h3 class="mt-4"><a href="<?php echo get_permalink() ?>"><?php the_title()?></a></h3>
                                    </div>
                                    <div class="col-md-12 meta">
                                        <p><small>Publicado em <?php echo get_the_date(); ?></small></p>
                                        <hr>
                                    </div>
                                    <div class="col-md-10 description">
                                        <p>
                                            <a href="<?php echo get_permalink() ?>">
                                                <?php echo wp_trim_words( get_the_content(), 50, '...' );?>
                                            </a>
                                        </p>
                                    </div>
                                    <div class="card-footer p-0 pt-3">
                                        <div class="col-md-12">
                                            <a href="<?php echo get_permalink() ?>" class="text-uppercase">leia mais ></a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4 p-0 text-center" >
                                    <a href="<?php echo get_permalink() ?>" class="img_link">    
                                        <span class="badge badge-primary d-none">Artigo</span>
                                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post), 'thumbnail' ); ?>
                                        <div class="col-md-12 p-0 bg_post" style="background-image: url('<?php echo $url; ?>');">

                                        </div>
                                    </a>
                                </div>
                                
                                
                            </div>
                        </div>
                    
                    </div>
                </div>