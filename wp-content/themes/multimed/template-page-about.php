<?php
/**
*
* Template Name: Institucional
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section id="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-10 text-center">
                <?php the_field( 'descricao', $page_ID ); ?>
            </div>
        </div>
    </div>
</section><!--/.content-->
<?php if( get_field('imagem_mapa', $page_ID) ): ?>

<section class="map">
    <div class="container-fluid p-0">
        <div data-toggle="modal" data-target="#exampleModalCenter">
            <img src='<?php the_field('imagem_mapa', $page_ID); ?>' class='img-fluid' alt='' title='' loading='lazy'>
</div>
        
    </div>
</section><!-- /.map -->
<!-- Modal -->
<div class="modal fade modal_map" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <?php the_field( 'mapa', $page_ID ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<section class="content bg_about">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-10 col-lg-8 text-center">
                <p><b><?php the_field( 'texto_chamada', $page_ID ); ?></b></p>
            </div>
        </div>
    </div>
</section><!-- /.content bg_about -->

<?php get_footer(); ?>