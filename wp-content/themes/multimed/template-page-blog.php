<?php
/**
*
* Template Name: Blog
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php
if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post blog_bg pb-3" style="background-image: url('<?php echo $featured_img_url;?>');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-end justify-content-center">
                            <div class="col-md-12 text-left">
                                <h1>
                                                   <?php the_field( 'subtitulo', $page_ID); ?>

                                </h1>
                            </div>
							<div class="col-md-8">
							<form id="searchform" method="get" action="<?php echo home_url('/'); ?>" class="w-100">
    <div class="input-group mb-3">
        <input type="text" class="search-field form-control" name="s" placeholder="Procurar" value="<?php the_search_query(); ?>" required="required">
        <input type="hidden" name="post_type[]" value="post" />
        <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">
                <button type="submit" value="Procurar"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</form>
</div>
                        </div>
                    </div>
                </section><!-- /.main -->
				        <nav class="navbar  fixed-bottom navbar-expand-sm navbar-light bg-light content" id="nav_bottom">
                    <div class="container h-100 h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
								<h4>
									Assine nossa newsletter
								</h4>
                                <?php echo do_shortcode('[contact-form-7 id="17" title="Formulário de contato 1"]');?>
                            </div>
                        </div>
                    </div>
                </nav>

	<section id="categories" class=" pb-2">
		<div class="container h-100">
			<div class="row m-0 h-100 align-items-center justify-content-between pt-3 pb-3">
				<div class="col-md-4 text-center mb-4 mb-md-0 text-md-left pl-lg-5">
					<h4>
						Categorias
					</h4>
					
				</div>
				<div class="col-md-8 text-center">
					<ul class="list-inline m-0">
						<li class="list-inline-item item mr-lg-5">
								<a href="/blog" class="link font-bariol" title=""> 
									Todos os posts 
									<hr /> 
								</a>
							</li>
						<?php 
							$categories = get_categories( array(
						    	'orderby' => 'name',
						    	'order'   => 'ASC'
							) );

						foreach( $categories as $category ) {
							?>
							
							<li class="list-inline-item item mr-lg-5 <?= $category->slug; ?>">
								<a href="<?php echo get_category_link( $category->term_id ); ?>" class="link font-bariol" title="<?php echo $category->name; ?>"> 
									<?php echo $category->name; ?> 
									<hr /> 
								</a>
							</li>
			
						<?php } ?>
					</ul>
					
				</div>
				
			</div>
		</div>
</section>
                <?php get_template_part( 'templates/global/template-part', 'list-posts' ); ?>


<?php get_footer(); ?>