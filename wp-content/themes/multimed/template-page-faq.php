<?php
/**
*
* Template Name: FAQ
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section class="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <?php if ( have_rows( 'cadastro_de_perguntas_e_respostas' ) ) : ?>
                <div class="col-md-12 text-left" id="accordion">
                    <?php $faq = 1; ?>
                    <?php while ( have_rows( 'cadastro_de_perguntas_e_respostas' ) ) : the_row(); ?>
                        <div class="card">
                            <div class="card-header <?php if ($faq == 1):?>active <?php endif;?>" id="heading<?php echo $faq;?>">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $faq;?>" aria-expanded="true" aria-controls="collapse<?php echo $faq;?>">
                                        <?php echo $faq;?>. <?php the_sub_field( 'pergunta' ); ?>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse<?php echo $faq;?>" class="collapse <?php if ($faq == 1):?>show <?php endif;?>" aria-labelledby="heading<?php echo $faq;?>" data-parent="#accordion">
                                <div class="card-body">
                                    <?php the_sub_field( 'resposta' ); ?>
                                </div>
                            </div>
                        </div>
                    <?php $faq++; endwhile; ?>
                </div>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
        </div>
    </div>
</section><!--/.content-->

<?php get_footer(); ?>

             