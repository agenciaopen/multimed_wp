<?php

/**
 *
 * Template Name: Política de Privacidade
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>


<section class="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-12">
                <h1 class="my-5 fw-bolder text-center">POLÍTICA DE PRIVACIDADE MULTIMED <br>SISTEMA DE BENEFICIOS S/A.</h1>
                <?php if (have_rows('cadastro_de_itens')) : ?>
                    <?php while (have_rows('cadastro_de_itens')) : the_row(); ?>
                    <h3 class="mb-3 mt-5 fw-normal"><i class="fas fa-chevron-circle-right"></i> <?php the_sub_field('titulo'); ?></h3>
                        <p class="text-justify"><?php the_sub_field('descricao', false, false); ?></p>
                        <?php if (have_rows('cadastro_de_subitens')) : ?>
                            <?php while (have_rows('cadastro_de_subitens')) : the_row(); ?>
                                <?php the_sub_field('titulo'); ?>
                                <p class="text-justify"><?php the_sub_field('descricao', false, false); ?></p>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found 
                            ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found 
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>