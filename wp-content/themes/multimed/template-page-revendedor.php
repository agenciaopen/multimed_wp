<?php
/**
*
* Template Name: Seja um revendedor
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section class="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-10 text-center">
                <?php the_field( 'formulario', $pageID ); ?>
            </div>
        </div>
    </div>
</section><!--/.content-->

<?php get_footer(); ?>